var phonecatApp = angular.module('phonecatApp', []);

// Define the `PhoneListController` controller on the `phonecatApp` module
phonecatApp.controller('PhoneListController', function PhoneListController($scope) {
  $scope.phones = [
    {
      name: 'Nexus S',
      snippet: 'Fast just got faster with Nexus S.'
    }, {
      name: 'Motorola XOOM™ with Wi-Fi',
      snippet: 'The Next, Next Generation tablet.'
    }, {
      name: 'MOTOROLA XOOM™',
      snippet: 'The Next, Next Generation tablet.'
    }
  ];
});


// var app = angular.module('jengDixi');

// app.config(function ($routeProvider, $locationProvider) {
//     $routeProvider.when('/', {
//         templateUrl: '../templates/login.html',
//         controller: 'LoginController'
//     });
// })
// .controller('LoginController', function ($scope){
// });


// var app = angular.module('jdTimeline', []);

// app
// .factory('socket', function ($rootScope) {
//   var socket = io.connect('http://localhost:3000');
//   return {
//     on: function (eventName, callback) {
//       socket.on(eventName, function () {  
//         var args = arguments;
//         $rootScope.$apply(function () {
//           callback.apply(socket, args);
//         });
//       });
//     },
//     emit: function (eventName, data, callback) {
//       socket.emit(eventName, data, function () {
//         var args = arguments;
//         $rootScope.$apply(function () {
//           if (callback) {
//             callback.apply(socket, args);
//           }
//         });
//       })
//     }
//   };
// })
// .controller('jdCtrl', function($scope, $timeout, socket) {
// 	$scope.showProfile = function(){
// 		$scope.class_profil_detail = "profil-detail-show";
// 		$scope.class_bar_statik = "bar-statik-hide";
// 		$scope.class_bar_judul_profile = "bar-judul-profile-show";
// 		$scope.class_bar_judul_home = "bar-judul-home-hide";
// 	}

// 	$scope.backBarJudulHome = function(){
// 		$scope.class_profil_detail = "";
// 		$scope.class_bar_statik = "";
// 		$scope.class_bar_judul_profile = "";
// 		$scope.class_bar_judul_home = "";
// 	}

// 	function init(){
		
//     	$scope.class_fab_shopink = "fab-base";
//     	$scope.class_fab_post = "fab-base";
//     	$scope.class_fab_forum = "fab-base";
//     	$scope.class_fab_delivery = "fab-base";
//     	$scope.class_fab_close = "fab-base";
    	
//     	$scope.class_fab_bag = "fab-bag-show";
// 	}

// 	init();

// 	$scope.hello = function(){
// 		socket.emit('pesan', 'halo');
// 	}

//     $scope.showFAB = function(){
//     	$scope.class_fab_shopink = "fab-shopink-trans";
//     	$scope.class_fab_post = "fab-post-trans";
//     	$scope.class_fab_forum = "fab-forum-trans";
//     	$scope.class_fab_delivery = "fab-delivery-trans";
//     	$scope.class_fab_close = "fab-close-trans";
//     	$scope.class_fab_bag = "fab-bag-hide";
//     	$timeout(function() {
// 	    	$scope.class_label_shopink = "fab-label-show";
// 	    	$scope.class_label_post = "fab-label-show";
// 	    	$scope.class_label_forum = "fab-label-show";
// 	    	$scope.class_label_delivery = "fab-label-show";
//         }, 250); // delay 250 ms
//     }

//     $scope.hideFAB = function(){
//     	$scope.class_label_shopink = "";
//     	$scope.class_label_post = "";
//     	$scope.class_label_forum = "";
//     	$scope.class_label_delivery = "";
//     	$scope.class_fab_shopink = "";
//     	$scope.class_fab_post = "";
//     	$scope.class_fab_forum = "";
//     	$scope.class_fab_delivery = "";
//     	$scope.class_fab_close = "";
//     	$scope.class_fab_bag = "fab-bag-show";
//     }
// });
